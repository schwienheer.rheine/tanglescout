class _Renderer{

	//> Creation!
	constructor(){
		
		this.graph = Storage.getGraph();
				
		//> Debug print config!
		this.printVerbosity = 0; //> Minimum level that a message needs to have to get printed!
		this.enableDP = false;
		
		//> Variables!
		this.renderer = new dagreD3.render(); //> Actually draws the graph!
		this.shouldRerender = false;
		this.svg = d3.select("svg"); //> Actual graphic object, with id "svg_visualization"!
		this.inner = this.svg.select("g"); //> Contains all the stuff!
		this.cfg = {address: {}, bundle: {}, transactionIN: {}, transactionOUT: {}}; //> Config for layout!
		
		//> Config for addresses!
		this.cfg.address.cornerRoundingRadius = 5;
		this.cfg.address.shape = "rect";
		this.cfg.address.width = 280;
		this.cfg.address.height = 19;
		
		//> Config for bundles!
		this.cfg.bundle.cornerRoundingRadius = 0;
		this.cfg.bundle.shape = "ellipse";
		this.cfg.bundle.width = 80;
		this.cfg.bundle.height = 35;
		
		//> Config for transactionIN!
		this.cfg.transactionIN.arrowhead = "vee";
		this.cfg.transactionIN.labelpos = "l";
		this.cfg.transactionIN.labeloffset = 10;
		
		//> Config for transactionOUT!
		this.cfg.transactionOUT.arrowhead = "vee";
		this.cfg.transactionOUT.labelpos = "l";
		this.cfg.transactionOUT.labeloffset = 10;
		
		//> Set graph properties !
		this.graph.setGraph({
			rankdir: "LR",
			align: "UR",
			marginx: 0,
			marginy: 0,
			nodesep: 100,
			edgesep: 100,
			ranksep: 100,
			ranker: "network-simplex"
		});
		
		//> Some text formats!
		this.textColorPositive = "#00cc00";
		this.textColorNegative = "#cc0000";
		
		//> Rendering variables!
		this.dpx = 0.5 * window.innerWidth; //> Draw point. This is the center of the svg that contains the graph!
		this.dpy = 0.5 * window.innerHeight;
		this.lasttx = 0;
		this.lastty = 0;
		this.curZoom = 1; //> Current zoom level!
		this.masterNode = undefined; //> Node that of node (address) that will always be on the drawing point!

		//> Set up zoom and pan support!
		this.zoom = d3.zoom()
			.scaleExtent([0.05, 1])
			.on("zoom", function(){
			if (d3.event.transform.k == this.curZoom){ //> Event is mouse movement!
				var dx = d3.event.transform.x - this.lasttx;
				var dy = d3.event.transform.y - this.lastty;
				this.dpx = this.dpx + dx;
				this.dpy = this.dpy + dy;
			} else { //> Event is scroll zooming!
				this.curZoom = d3.event.transform.k;
			};
			this.lasttx = d3.event.transform.x;
			this.lastty = d3.event.transform.y;
			this.inner.attr("transform", "translate(" + this.getGraphTranslateX(this.graph) + ", " + this.getGraphTranslateY(this.graph) + ") scale(" + this.curZoom + ")");
		}.bind(this));
		this.svg.call(this.zoom);
		
		//> Set svg to fullscreen!
		this.svg.attr("height", window.innerHeight);
		this.svg.attr("width", window.innerWidth);

		this.dp("_Renderer instance created!", 5);
	}; //> END constructor!
		
	//> Update call!
	update(){
//console.log(Storage.getBufferLength(), Storage.getNextCount())	
	if (Storage.getBufferLength() == 0){
			//> Fetch item from storage out queue!
			var nxt = Storage.getNext();
			if(nxt){
				if (nxt.type == "address" || nxt.type == "bundle"){
					this.setSingleNode(nxt.hash);
				} else if (nxt.type == "transactionIN" || nxt.type == "transactionOUT"){
					this.setSingleEdge(nxt.fromHash, nxt.toHash);
				};
			}else if(this.shouldRerender == true && Storage.getBufferLength() == 0){
				this.shouldRerender = false;
				this.rerender();
			};
			
	};
				
	};
		
		
	//> (Re-)renders the Graph. Quite a heavy function, don't call too often!
	rerender(){

		//> Reset zoom before redrawing!
		this.inner.attr("transform", "translate(" + this.getGraphTranslateX(this.graph) + ", " + this.getGraphTranslateY(this.graph) + ") scale(1)");

		//> Trigger setting dagre layout. Adds position to all nodes, edges, etc!
		dagre.layout(this.graph);

		//> Run the renderer. This is what draws the final graph!
		this.renderer(this.inner, this.graph);

		//> Move graph such that the masterNode center coords are right on the current drawing point!
		this.inner.attr("transform", "translate(" + this.getGraphTranslateX() + ", " + this.getGraphTranslateY() + ") scale(" + this.curZoom + ")");
		
		this.dp("_Renderer finished rendering call!", 5);
	}; //> END update()!
	
	//> Returns [AAAAA...AAAAA] for label for given hash!
	hashToLabel(v){
		return "[" + v.substring(0, 4) + " ... " + v.substring(v.length - 4, v.length) + "]";
	};
	
	doRender(){
		this.shouldRerender = true;
	};
	
	setSingleNode(hash){
		var node = this.graph.node(hash);
		var nodeCfg = this.cfg[node.type];
		node.rx = node.ry = nodeCfg.cornerRoundingRadius; //> Round node corners!
		node.width = nodeCfg.width;
		node.height = nodeCfg.height;
		node.shape = nodeCfg.shape;
		if (node.type == "address"){
			var html = "<div class=div_addressLabel>";
			html += "<div class=div_addressExpandLeft onclick=UI.node_expandBackward(\"" + hash.toString() + "\")></div>";
			html += "<div class=div_addressMiddle><a target=\"_blank\" href=\"https://thetangle.org/address/" + hash + "\">" + this.hashToLabel(hash) + "</a></div>";
			
			
			if (Search.hasQuery(hash, "forward")){
			
			
				html += "<div class=\"div_addressExpandRightWorking\" onclick=UI.node_expandForward(\"" + hash.toString() + "\")></div>";
			
			} else {
				html += "<div class=\"div_addressExpandRight\" onclick=UI.node_expandForward(\"" + hash.toString() + "\")></div>";
			};
			
			
			
			
			html += "</div>";
			node.label = html;
			node.labelType = "html";
		} else { //> Node is a bundle!
			node.label = "<div class=bundleLabel><a target=\"_blank\" href=\"https://thetangle.org/bundle/" + hash + "\">" + this.timestampToDateString(node.timestamp) + "</a></div>";
			node.labelType = "html";
		};
	};
	
	setSingleEdge(fromHash, toHash){
		var edge = this.graph.edge(fromHash, toHash);
		var edgeCfg = this.cfg[edge.type];
		var tc = this.valueToTextColor(edge.value);
		edge.arrowhead = edgeCfg.arrowhead;
		edge.arrowheadStyle = "fill: " + tc;
		edge.label = "<a target=\"_blank\" href=\"" + "https://thetangle.org/transaction/" + edge.hash + "\"><font onMouseOut=this.style.color=\"" + tc + "\" onMouseOver=this.style.color=\"#0000cc\" color=\"" + tc + "\">" + this.valueToLabel(edge.value) + "</font></a>";
		edge.labelpos = edgeCfg.labelpos;
		edge.labeloffset = edgeCfg.labeloffset;
		edge.curve = d3.curveBasis;
		edge.style = "stroke: " + tc + "; fill: none; stroke-width: " + this.valueToStrokeWidth(edge.value) + "px;";
		edge.labelStyle = "color: " + tc + "; font-size: " + 2 * this.valueToFontSize(edge.value) + "px;";
		edge.labelType = "html";
	}


	
	setMasterNode(v){
		this.masterNode = v;
	};
	
	initializeMasterNode(v){
		if (this.masterNode == undefined){
			this.masterNode = v;
		};
	};
	
	getGraphTranslateX(){
		if (this.masterNode == undefined || this.graph.node(this.masterNode).x == undefined){
			return 0;
		};
		return Math.floor(this.dpx - this.curZoom * this.graph.node(this.masterNode).x);
	};
	
	getGraphTranslateY(){
		if (this.masterNode == undefined || this.graph.node(this.masterNode).y == undefined){
			return 0;
		};
		return Math.floor(this.dpy - this.curZoom * this.graph.node(this.masterNode).y);
	};
	
	//> Returns shortened "xGi" label given full amount!
	valueToLabel(v){
		
		//> Check if negative!
		var minus = "";
		if (v < 0){
			minus = "-";
		};
		v = Math.abs(v);
		
		//> Find shorthand!
		var sh = "";
		if (v >= 1000000000000000){
			sh = "Pi";
		} else if (v >= 1000000000000){
			sh = "Ti";
		} else if (v >= 1000000000){
			sh = "Gi";
		} else if (v >= 1000000){
			sh = "Mi";
		} else if (v >= 1000){
			sh = "Ki";
		} else {
			sh = "i";
		};
		
		//> Shorten value!
		while (v >= 1000){
			v = 0.001 * v;
		};
		
		return (minus + (Math.round((v + 0.00001) * 100) / 100) + sh);
	};
	
	//> Turns timestamp to date string!
	timestampToDateString(t){
		var d = new Date(1000 * t);
		var month = d.getMonth();
		var day = d.getDate();
		var year = d.getFullYear();
		var hour = d.getHours();
		var min = d.getMinutes();
		hour = (hour < 10) ? ("0" + hour) : hour;
		min = (min < 10) ? ("0" + min) : min;
		var ma = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		return ma[month] + " " + day + ", " + year + "<br>" + hour + ":" + min;
	};
	
	//> Returns color depending on value pos or neg!
	valueToTextColor(v){
		if (v >= 0){
			return this.textColorPositive;
		} else {
			return this.textColorNegative;
		};
	};
	
	//> Turns value to stroke width!
	valueToStrokeWidth(v){
		v = Math.abs(v);
		if (v >= 1000000000000000){ //> Pi!
			return 16;
		} else if (v >= 1000000000000){ //> Ti!
			return 13;
		} else if (v >= 100000000000){ //> Gi!
			return 9;
		} else if (v >= 10000000000){
			return 8;
		} else if (v >= 1000000000){
			return 8;
		} else if (v >= 100000000){ //> Mi!
			return 7;
		} else if (v >= 10000000){ //> Mi!
			return 7;
		} else if (v >= 1000000){ //> Mi!
			return 6;
		} else if (v >= 1000){ //> Any ki!
			return 3;
		} else { //> Anything below 1000i!
			return 1;
		};
	};
	
	//> Turns value to font size!
	valueToFontSize(v){
		v = Math.abs(v);
		if (v >= 1000000000000000){ //> Pi!
			return 16;
		} else if (v >= 1000000000000){ //> Ti!
			return 14;
		} else if (v >= 100000000000){ //> Gi!
			return 11;
		} else if (v >= 10000000000){
			return 10;
		} else if (v >= 1000000000){
			return 10;
		} else if (v >= 100000000){ //> Mi!
			return 9;
		} else if (v >= 10000000){ //> Mi!
			return 8;
		} else if (v >= 1000000){ //> Mi!
			return 8;
		} else if (v >= 1000){ //> Any ki!
			return 6;
		} else { //> Anything below 1000i!
			return 6;
		};
	};
	
	//> Debug Print!
	dp(txt, lvl=99){
		if(this.enableDP){
			if(lvl >= this.printVerbosity){
				console.log(txt);
			};
		};		
	};
	
}; //> END class!