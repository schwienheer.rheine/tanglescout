class _UI{

	//> Creation!
	constructor(){

	}; //> END constructor!
		
	btn_donate(){
		var x = document.getElementById("div_donate");
		var s = getComputedStyle(x, null).display;
		if (s === "none") {
			x.style.display = "block";
		} else {
			x.style.display = "none";
		}
	};
	
	btn_addAddress(){
		var y = document.getElementById("btn_addAddress");
		var z = document.getElementById("btn_addAddressClose");
		var x = document.getElementById("div_enterAddress");
		var s = getComputedStyle(x, null).display;
		if (s === "none") {
			x.style.display = "block";
			y.innerHTML = "Add"
			y.style.width = "66px";
			z.style.display = "block";
		} else {
			Search.addStartingAddress(x.innerHTML.replace(/\s/g, "").substring(0, 81));
			this.btn_addAddressClose();
		}
	};
	
	btn_addAddressClose(){
		var y = document.getElementById("btn_addAddress");
		var z = document.getElementById("btn_addAddressClose");
		var x = document.getElementById("div_enterAddress");
		var s = getComputedStyle(x, null).display;
		x.style.display = "none";
		y.innerHTML = "Add Address ...";
		z.style.display = "none";
		y.style.width = "132px";	
		x.innerHTML = "";
	};
	
	node_expandForward(a){
		Search.addStartingAddress(a, "forward");
//		Renderer.setSingleNode(a);
//		Renderer.rerender();
	};
	
	node_expandBackward(a){
		Search.addStartingAddress(a, "backward");
	};

};