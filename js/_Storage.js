/*
	//> Provides:
	
		p	constructor()
		p	update()
		p	add(type="", hash="", value="", fromHash="", toHash="", timestamp="")
		
		p	getGraph()
		p	getBufferLength()
		p	getNodeCount()
		p	getLinkCount()
		p	getTotalCount()
		
		p	getMinDepth()
		p	getMaxDepth()
		p	getDepthesCount()
		
		p	hasNode(hash)
		p	hasLink(hash)
		p	hasHash(hash)
		
		i	addLink(e)
		i	addNode(e)
		i	updateDepth(v)
		i	updateValues(i, v)
		i	dp(t)
		
	//> Expected nodes/links to add:

		type: transactionIN, transactionOUT, address, bundle
		hash: hash
		depth: categorical distance from start address
		value: value in IOTAs
		fromHash: for links, starting hash
		toHash: for links, ending hash

*/

class _Storage{

	//> Creation!
	constructor(){

		//> Config!
		this.debugPrint = false; //> Debug print toggle!
		
		//> Variables!
		this.buffer = new Queue(); //> Buffers nodes and edges to be added!
		this.out = new Queue();
			
		//> Main container!
		this.G = new dagreD3.graphlib.Graph({directed: true, compound: false, multigraph: false})
			.setGraph({})
			.setDefaultEdgeLabel(function(){return {};});

		this.dp("_Storage instance created!");
	}; //> END constructor!
		
	//> Update!
	update(){
	
		//> Check if buffer is empty!
		if (!this.buffer.isEmpty()){
				
			//> Fetch and abort if already known!
			var e = this.buffer.dequeue();

			//> Continue!
			this.dp("_Storage: Adding hash " + e.hash + "!");
			if (e.type == "transactionIN" || e.type == "transactionOUT"){ //> Link!
							
				//> Abort and re-enqueue if link connects unknown address/bundle pair!
				if ((!this.hasNode(e.fromHash)) || (!this.hasNode(e.toHash))){
					this.buffer.enqueue(e);
					this.dp("_Storage: Aborting addition because link connects unknowns!");
				} else {
					
					//> Add link!
					this.dp("_Storage: It's a valid link!");
					this.addLink(e);
					this.out.enqueue({"fromHash": e.fromHash, "toHash": e.toHash, "type": e.type});
				};
				
			} else { //> Address or bundle!
				
				//> Add node!
				this.dp("_Storage: It's a valid node!");
				this.addNode(e);
				this.out.enqueue({"hash": e.hash, "type": e.type});
				
			};

		}; //> END buffer.isEmpty()!
	}; //> END update()!
		
	//> Adds a node (address or bundle) or a link to the buffer!
	add(type="", hash="", value=0, fromHash="", toHash="", timestamp=0){
		if (!this.hasHash(hash)){
			this.buffer.enqueue({type: type, hash: hash, value: value, fromHash: fromHash, toHash: toHash, timestamp: timestamp});
			this.dp("_Storage: Hash enqueued!");
			return true;
		};
		this.dp("_Storage: Hash already known!");
		return false;
	}; //> END add()!
	
	//> Getters!
	getGraph(){
		return this.G;
	};
	getBufferLength(){
		return this.buffer.getLength();
	};
	getNodeCount(){
		return this.G.nodeCount();
	};
	getLinkCount(){
		return this.G.edgeCount();
	};
	getTotalCount(){
		return this.getNodeCount() + this.getLinkCount();
	};
	hasNode(hash){
		return (this.G.node(hash) != undefined);
	};
	hasLink(hash){
		return (this.G.edge(hash) != undefined);
	};
	hasHash(hash){
		return (this.hasNode(hash) || this.hasLink(hash));
	};
	getNext(){
		return this.out.dequeue();
	}
	getNextCount(){
		return this.out.getLength();
	};
	
	//> Adds link to graph!
	addLink(e){
		this.G.setEdge(e.fromHash, e.toHash, {hash:	e.hash,
										 type: e.type,
										 value:	e.value});
		if (e.type == "transactionIN"){ //> Add timestamp to the bundle!
			var n = this.G.node(e.fromHash);
			n.timestamp = e.timestamp;
		} else { //> txOUT!
			var n = this.G.node(e.toHash);
			n.timestamp = e.timestamp;
		};
	};
	
	//> Add node to graph!
	addNode(e){
		this.G.setNode(e.hash, {type: e.type});		
	};
	
	//> Debug print!
	dp(t){
		if (this.debugPrint){
			console.log(t);
		};
	};
	
}; //> END class!