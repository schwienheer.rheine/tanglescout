class _Search{

	//> Creation!
	constructor(){
		
		//> Variables!
		this.queries = {};
		
		//> Debug print config!
		this.printVerbosity = 0; //> Minimum level that a message needs to have to get printed!
		this.enableDP = false;
		this.dp("_Search instance created!", 5);
		
	}; //> END constructor!
		
	//> Adds a new starting point search address!
	addStartingAddress(a, direction=undefined){
		if (this.isAddress(a)){
			if (Storage.add("address", a) == true){
				Renderer.setMasterNode(a);
				Renderer.doRender();
			};
			if (direction != undefined){ //> Add query!
				if (!(a in this.queries)){
					this.queries[a] = {"forward": [], "backward": []};
				};
				this.queries[a][direction].push(new _Query(a, undefined, direction, undefined, undefined));
			};
		};
	};
	
	hasQuery(a, direction){
		if (a in this.queries){
			if (this.queries[a][direction].length > 0){
				return true;
			};
			return false;
		};
		return false;
	};
		
	//> Update call!
	update(){
		for (var hash in this.queries){
			for (var i = this.queries[hash]["forward"].length - 1; i >= 0; i--){
				this.updateQuery(hash, "forward", this.queries[hash]["forward"][i], i);
			};
			for (var j = this.queries[hash]["backward"].length - 1; j >= 0; j--){
				this.updateQuery(hash, "backward", this.queries[hash]["backward"][j], j);
			};
		};
	};
	
	updateQuery(h, d, q, i){
		q.update();
		
		//> Check if query is finished and resolve it!
		if (Storage.getNextCount() == 0 && Storage.getBufferLength() == 0 && q.isResolveable()){
			var out = q.resolve();
			if (out.length > 0){
				Renderer.doRender();
			};
			out.forEach(function(e){
				Storage.add(e["type"], e["hash"], e["value"], e["from"], e["to"], e["timestamp"]);
			}.bind(this));
			this.queries[h][d].splice(i, 1);
		};
	};
		
	//> Returns true if string is a valid address, false else!
	isAddress(str){
		
		//> Address must be 90 characters (or 81, if without checksum) long!
		if(!(str.length == 81)){
			return false;
		};
		
		//> Make sure address consists only of [A-Z, 9]!
		if(!(/^[A-Z9]+$/.test(str))){
			return false;
		};
		
		return true;
	};

	//> Debug Print!
	dp(txt, lvl=99){
		if(this.enableDP){
			if(lvl >= this.printVerbosity){
				console.log(txt);
			};
		};		
	};
	
}; //> END class!