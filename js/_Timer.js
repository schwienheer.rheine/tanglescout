class _Timer{

	//> Creation!
	constructor(duration=1000){

		//> Config!
		this.debugPrint = false; //> Debug print toggle!
		
		//> Parameters!
		this.duration = duration;
		this.startTime = Date.now();
		this.done = false;
		
		this.dp("_Timer instance created!");
	}; //> END constructor!
		
	//> Update!
	update(){
		if (!this.done && Date.now() >= this.startTime + this.duration){
			this.done = true;
			this.dp("_Timer DONE!");
		};
	
	}; //> END update()!
	
	getDuration(){
		return this.duration;
	};
	
	setDuration(v){
		this.duration = v;
	};
	
	reset(){
		this.done = false;
		this.startTime = Date.now();
	};
	
	isDone(){
		return this.done;
	};
	
	//> Debug print!
	dp(t){
		if (this.debugPrint){
			console.log(t);
		};
	};
	
}; //> END class!